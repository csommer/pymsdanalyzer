import numpy
import pandas
from matplotlib import cm
from sklearn.cluster import DBSCAN
from sklearn.metrics import pairwise_distances
from scipy.spatial.distance import squareform

from . import msd, utils, velocity

class TrajectoryFeature(object):
    def __init__(self, coords, frame):
        self.coords = coords
        self.frame = frame


class MSDParabola(TrajectoryFeature):
    name = "MSD_Parabola"
    def compute(self, trj, min_trj_len=3, clip=0.9):
        tau, msd_vals = msd.msd_per_track(trj, self.coords, self.frame)
        d, v2, r2 = 0, 0, 0
        if len(tau) > min_trj_len:
            d, v2, r2 = utils.fit_parabola(tau, msd_vals, clip=clip)

        return pandas.Series({
                              "MSDparabola_d" : d,
                              "MSDparabola_v2": v2,
                              "MSDparabola_r2": r2
                              })

class VACstats(TrajectoryFeature):
    name = "VAC_stats"
    def compute(self, trj, min_trj_len=3):
        displacements = velocity.compute_velocities(trj, self.coords, self.frame)
        tau, vac_vals = velocity.velocity_autocorr_pre_track(displacements, self.coords, self.frame)

        if len(tau) > min_trj_len:
            return pandas.Series({"vac_mean": vac_vals[1:].mean(),
                                  "vac_std": vac_vals[1:].std(),
                                  "vac_min": vac_vals[2:].min(),
                                  "vac_max": vac_vals[2:].max()})
        else:
            return pandas.Series({"vac_mean": 0, "vac_std": 0, "vac_min": 0, "vac_max": 0})


class SpeedStats(TrajectoryFeature):
    name = "Speed_stats"
    def compute(self, trj, min_trj_len=2):
        displacements = velocity.compute_velocities(trj, self.coords, self.frame)

        speeds = numpy.linalg.norm(displacements[self.coords], axis=1) / trj[self.frame].diff().dropna()


        if len(speeds) > min_trj_len:
            speed_std = speeds.std()
        else:
            speed_std = -1

        return pandas.Series({"speed_mean": speeds.mean(),
                              "speed_std": speed_std,
                              "speed_min": speeds.min(),
                              "speed_max": speeds.max()})

class ConfinementRatio(TrajectoryFeature):
    name = "ConfinementRatio"
    def compute(self, trj):
        return pandas.Series({"confinment_ratio": numpy.linalg.norm(trj.iloc[-1][self.coords]
                                                                    - trj.iloc[0][self.coords])
                                                                    / numpy.linalg.norm(trj[self.coords].diff().dropna(), axis=1).sum()})

class MeanStraightLineSpeed(TrajectoryFeature):
    name = "MeanStraightLineSpeed"
    def compute(self, trj):
        return pandas.Series({"mean_straight_line_speed": numpy.linalg.norm(trj.iloc[0][self.coords]
                                                                               - trj.iloc[-1][self.coords])
                                                                            / (trj.iloc[-1][self.frame]
                                                                               - trj.iloc[0][self.frame])})

class EllipseFit(TrajectoryFeature):
    name = "EllipseFit"
    def compute(self, trj):
        p = trj[self.coords].values
        pc = p - p.mean(0)

        cov = numpy.cov(pc.T)
        evals, evecs = numpy.linalg.eig(cov)

        sort_indices = numpy.argsort(evals)

        return pandas.Series({"ellipse_minor" : evals[sort_indices[0]], "ellipse_major" : evals[sort_indices[1]]})

class TrackDuration(TrajectoryFeature):
    name = "track_duration"
    def compute(self, trj):
        return pandas.Series({"track_duration": (1 + trj.iloc[-1][self.frame] - trj.iloc[0][self.frame])})

class TrackLength(TrajectoryFeature):
    name = "track_length"
    def compute(self, trj):
        return pandas.Series({"track_length": numpy.linalg.norm(trj[self.coords].diff().dropna(), axis=1).sum()})

class TrackDiameter(TrajectoryFeature):
    name = "track_diameter_stats"
    def compute(self, trj):
        pw = pairwise_distances(trj[self.coords].values)
        sf = squareform(pw, checks=False)

        return pandas.Series({"diameter_mean"  : sf.mean(),
                              "diameter_max"   : sf.max(),
                              "diameter_std"   : sf.std(),
                             })



class PartitionFeature(TrajectoryFeature):
    name = "PartitionFeatures"
    def compute(self, trj, eps=6, t_scale=1.):

        p = trj[self.coords].values
        t = trj[self.frame].values
        dt = numpy.diff(t)

        labels = partition_dbscann(p, t, eps, t_scale=t_scale, ) + 1

        from skimage.measure import label
        from scipy.ndimage.morphology import binary_dilation
        from skimage.morphology import remove_small_objects

        sub_track_indecies = []
        unique_labels = set(labels)
        for u in unique_labels:
            if u > 0:
                sub_track_indecies.append(numpy.nonzero(labels == u)[0])

        sub_track_center_idx = [int(sti.mean()) for sti in sub_track_indecies]
        sub_track_center_pos = [p[c, :] for c in sub_track_center_idx]
        sub_track_center_time = [t[c] for c in sub_track_center_idx]

        if len(unique_labels) > 2:
            between_times = numpy.diff(sub_track_center_time)
            between_dists = numpy.linalg.norm(numpy.diff(sub_track_center_pos, axis=0), axis=1)

        else:
            between_times = -1
            between_dists = -1

        res = {
                "Partition_dwell_count" : labels.max(),
                "Partition_dwell_duration" : (dt * (labels[1:] > 0)).sum(),
                "Partition_move_duration"  : (dt * (labels[1:] == 0)).sum(),
                "Partition_between_dwell_mean_time" : numpy.mean(between_times),
                "Partition_between_dwell_max_time"  : numpy.max(between_times),
                "Partition_between_dwell_mean_dist" : numpy.mean(between_dists),
                "Partition_between_dwell_max_dist"  : numpy.max(between_dists),
                }

        labels = labels==0
        labels = remove_small_objects(labels, 3)
        labels = label(labels)


        sub_track_indecies = []
        unique_labels = set(labels)
        for u in unique_labels:
            if u > 0:
                sub_track_indecies.append(numpy.nonzero(labels == u)[0])

        sub_track_pos  = [p[sti, :] for sti in sub_track_indecies]
        sub_track_time = [t[sti] for sti in sub_track_indecies]

        sub_trajectoris = [pandas.DataFrame({self.coords[0] : p[:,0], self.coords[1]:p[:,1], self.frame:t}) for p,t in zip(sub_track_pos, sub_track_time)]


        SS = SpeedStats(self.coords, self.frame)
        if len(sub_trajectoris) > 0:
            speed_vals = pandas.concat([SS.compute(st) for st in sub_trajectoris if len(st) > 1], axis=1)
            for k, (sm, ss) in enumerate(zip(speed_vals.mean(1), speed_vals.std(1))):
                res["MovingTracklet_{}_{}_mean".format(SS.name, k)] = sm

        else:
            for k, v in enumerate(range(4)):
                res["MovingTracklet_{}_{}_mean".format(SS.name, k)] = -1


        ELF = EllipseFit(self.coords, self.frame)
        if len(sub_trajectoris) > 0:
            elf_vals = pandas.concat([ELF.compute(st) for st in sub_trajectoris if len(st) > 1], axis=1)
            for k, (sm, ss) in enumerate(zip(elf_vals.mean(1), elf_vals.std(1))):
                res["MovingTracklet_{}_{}_mean".format(ELF.name, k)] = sm

        else:
            for k, v in enumerate(range(2)):
                res["MovingTracklet_{}_{}_mean".format(ELF.name, k)] = -1


        VS = VACstats(self.coords, self.frame)
        if len(sub_trajectoris) > 0:
            vs_vals = pandas.concat([VS.compute(st,  min_trj_len=3) for st in sub_trajectoris if len(st) > 1], axis=1)
            for k, (sm, ss) in enumerate(zip(vs_vals.mean(1), vs_vals.std(1))):
                res["MovingTracklet_{}_{}_mean".format(VS.name, k)] = sm

        else:
            for k, v in enumerate(range(4)):
                res["MovingTracklet_{}_{}_mean".format(VS.name, k)] = -1

        MP = MSDParabola(self.coords, self.frame)
        if len(sub_trajectoris) > 0:
            mp_vals = pandas.concat([MP.compute(st,  min_trj_len=3) for st in sub_trajectoris if len(st) > 1], axis=1)
            for k, (sm, ss) in enumerate(zip(mp_vals.mean(1), mp_vals.std(1))):
                res["MovingTracklet_{}_{}_mean".format(MP.name, k)] = sm
        else:
            for k, v in enumerate(range(3)):
                res["MovingTracklet_{}_{}_mean".format(MP.name, k)] = -1

        return pandas.Series(res)



def partition_dbscann(p, t, eps, t_scale=1.):
    X = numpy.c_[p, t * t_scale]
    clustering = DBSCAN(eps).fit(X)
    labels = clustering.labels_

    return labels


def partition_get_moving_part(trj, eps, t_scale, coords, frame):

    p = trj[coords].values
    t = trj[frame].values
    dt = numpy.diff(t)

    labels = partition_dbscann(p, t, eps, t_scale=t_scale, ) + 1

    from skimage.measure import label
    from scipy.ndimage.morphology import binary_dilation
    from skimage.morphology import remove_small_objects



    labels = labels==0
    labels = remove_small_objects(labels, 3)

    new_trj = trj[labels]
    new_trj[frame] = list(range(len(new_trj)))

    if len(new_trj) == 0:
        print("aASDFASDFASDF")

    return new_trj[coords + [frame]]






